import { createContext, Dispatch } from "react";
import { User } from "./entities/User.entity";
import { Song } from "./entities/Song.entity";
import { Playlist } from "./entities/Playlist.entity";


export interface AppContextData {
  isSongPlaying?: boolean;
  setIsSongPlaying?: Dispatch<React.SetStateAction<boolean>>;
  
  isNowPlayingViewOpen?: boolean;
  setIsNowPlayingViewOpen?: Dispatch<React.SetStateAction<boolean>>;
  
  loggedInUser?: User;
  setLoggedInUser?: Dispatch<React.SetStateAction<User>>;
  
  currentSong?: Song;
  setCurrentSong?: Dispatch<React.SetStateAction<Song>>;

  currentPlaylistName?: string;
  setCurrentPlaylistName?: Dispatch<React.SetStateAction<string>>;
  
}


export const AppContextDefaultData = {};


export const AppContext = createContext<AppContextData>(AppContextDefaultData);
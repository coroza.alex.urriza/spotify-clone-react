import { useContext } from 'react';
import { Button } from 'primereact/button';
import { AspectRatio } from 'react-aspect-ratio';
import CustomScroll from 'react-custom-scroll';

import './NowPlayingPanel.scss';
import { AppContext } from '../../AppContext';
import { NextInQueue } from '../../components/next-in-queue/NextInQueue';
import { ArtistTourCard } from '../../components/artist-tour-card/ArtistTourCard';
import { useGetArtistsTextFromSong } from '../../hooks/useGetArtistsTextFromSong';



export function NowPlayingPanel() {
  const { setIsNowPlayingViewOpen, currentSong, currentPlaylistName } = useContext(AppContext);
  const artistsText = useGetArtistsTextFromSong(currentSong);
  const mainArtist = currentSong?.artists?.at(0);


  const renderIsVerifiedArtist = () => {
    if(mainArtist?.isVerified) {
      return (
        <div className="is-verified flex flex-row gap-2 align-items-center">
          <span className="pi pi-check-circle text-2xl"></span>
          <div className="text-sm">Verified Artist</div>
        </div>
      )
    } else {
      return <div></div>
    }
  }
  
  
  return (
    <CustomScroll heightRelativeToParent="100%" allowOuterScroll={true}>
      <div className="NowPlayingPanel flex flex-column gap-3">
        <div className="header flex flex-row justify-content-between align-items-center">
          <div className="now-playing-title">{currentPlaylistName}</div>
          <Button icon="pi pi-times" title="Close" rounded text aria-label="Cancel" onClick={() => setIsNowPlayingViewOpen(false)} />
        </div>


        <AspectRatio className="song-photo-container">
          <img src="https://picsum.photos/300" />
        </AspectRatio>


        <div className="song-details flex flex-row justify-content-between align-content-center">
          <div className="flex flex-column gap-1">
            <div className="title text-xl">{currentSong?.title}</div>
            <div className="artist text-sm">{artistsText}</div>
          </div>

          <div className="actions flex flex-row gap-2 align-items-center">
            <Button icon="pi pi-heart" title="Save to Your Library" rounded text />
            <Button icon="pi pi-ellipsis-h" title={`More Options for Tensionado`} rounded text />
          </div>
        </div>


        <AspectRatio className="artist-photo-container">
          <img src="https://picsum.photos/301" />
          <div className="overlay flex flex-column justify-content-between">
            {renderIsVerifiedArtist()}

            <div className="other-details flex flex-column gap-1 text-sm">
              <div className="monthly-listeners">{mainArtist?.monthlyListenersCount} monthly listeners</div>
              <div className="description">{mainArtist?.introText}</div>
            </div>
          </div>
        </AspectRatio>

        <ArtistTourCard tours={mainArtist?.tours}></ArtistTourCard>

        <NextInQueue nextSong={null}></NextInQueue>
      </div>
    </CustomScroll>
  );
}
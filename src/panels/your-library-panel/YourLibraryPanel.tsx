import './YourLibraryPanel.scss';
import { useContext, useState } from 'react';

import { PlaylistType } from '../../enums/PlaylistType.enum';
import { AppContext } from '../../AppContext';

import { LabelChip } from '../../ui-components/label-chip/LabelChip';
import { LibraryPlaylistItem } from '../../components/library-playlist-item/LibraryPlaylistItem';



export function YourLibraryPanel() {
  const [selectedPlaylistType, setSelectedPlaylistType] = useState(PlaylistType.All);
  const { loggedInUser } = useContext(AppContext);

  const isPlaylistTypeSelected = (type: PlaylistType): boolean => {
    if(type === selectedPlaylistType) {
      return true;
    } else {
      return false;
    }
  }


  const renderPlaylists = () => {
    const pinnedPlaylistIds = loggedInUser?.pinnedPlaylists?.map(playlist => playlist.id);
    const sortedPlaylists = loggedInUser?.likedPlaylists?.sort((a) => pinnedPlaylistIds.includes(a.id) ? -1 : 1); /** Pinned playlists should appear at the top of the list. NOTE: Not sure if this sorting function is correct. I just guessed the code and it seems to be working lol. */
    
    const playlistsFilteredByType = sortedPlaylists?.filter(playlist => {
      if((playlist.type === selectedPlaylistType) || selectedPlaylistType === PlaylistType.All) {
        return playlist;
      }
    });
    
    const playlists = playlistsFilteredByType?.map((playlist, index) => {
      return <LibraryPlaylistItem key={index} playlist={playlist} pinnedPlaylistIds={pinnedPlaylistIds}></LibraryPlaylistItem>;
    });

    return (playlists?.length) ? playlists : (<div className="font-italic text-sm">No playlists found</div>);
  }
  
  
  return (
    <div className="YourLibraryPanel flex flex-column gap-4">
      <div className="top-bar flex flex-row gap-3 align-items-center">
        <span className="icon material-symbols-outlined text-3xl">library_music</span>
        <div>Your Library</div>
        <div className="spacer flex-grow-1"></div>
        <span className="icon material-symbols-outlined">add</span>
        <span className="icon material-symbols-outlined">arrow_forward</span>
      </div>


      <div className="playlist-type-selector flex flex-row flex-wrap gap-2">
        <LabelChip isSelected={isPlaylistTypeSelected(PlaylistType.All)} onClick={() => { setSelectedPlaylistType(PlaylistType.All) }}>All</LabelChip>
        <LabelChip isSelected={isPlaylistTypeSelected(PlaylistType.Playlist)} onClick={() => { setSelectedPlaylistType(PlaylistType.Playlist) }}>Playlists</LabelChip>
        <LabelChip isSelected={isPlaylistTypeSelected(PlaylistType.Artist)} onClick={() => { setSelectedPlaylistType(PlaylistType.Artist) }}>Artists</LabelChip>
        <LabelChip isSelected={isPlaylistTypeSelected(PlaylistType.Album)} onClick={() => { setSelectedPlaylistType(PlaylistType.Album) }}>Albums</LabelChip>
      </div>


      <div className="playlists flex flex-column">
        { renderPlaylists() }
      </div>
    </div>
  );
}
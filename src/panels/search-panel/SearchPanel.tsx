import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './SearchPanel.scss';


type LinkType = ''|'home'|'search';



export function SearchPanel() {
  const [activeLink, setActiveLink] = useState('home');
  const navigate = useNavigate();
  
  /**
   * Gives yout the "active" class name if the link matches the currently active link.
   */
  const isLinkActive = (linkType: LinkType): string => {
    return `${(linkType === activeLink) ? 'active' : ''}`;
  }
  
  
  return (
    <div className="SearchPanel flex flex-column gap-4">
      <div className={`link flex flex-row align-items-center gap-3 ${isLinkActive('home')}`} onClick={() => { setActiveLink('home'); navigate('/') }}>
        <span className="icon material-symbols-outlined text-3xl">home</span>
        <div className="label">Home</div>
      </div>

      <div className={`link flex flex-row align-items-center gap-3 ${isLinkActive('search')}`} onClick={() => { setActiveLink('search'); navigate('/search') }}>
        <span className="icon material-symbols-outlined text-3xl">search</span>
        <div className="label">Search</div>
      </div>
    </div>
  );
}
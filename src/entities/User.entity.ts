import { Artist } from "./Artist.entity";
import { Playlist } from "./Playlist.entity";
import { Song } from "./Song.entity";



export interface User {
    id?: number;
    fullName?: string;
    photo?: string;
    ownPlaylists?: Playlist[];
    likedPlaylists?: Playlist[];
    pinnedPlaylists?: Playlist[];
    followedArtists?: Artist[];
    likedSongs?: Song[];
}
import { Album } from "./Album.entity";
import { ArtistTour } from "./ArtistTour.entity";


export interface Artist {
    id?: number;
    name?: string;
    photo?: string;
    coverPhoto?: string;
    introText?: string;
    followersCount?: number;
    isVerified?: boolean;
    monthlyListenersCount?: number;
    facebook?: string;
    instagram?: string;
    twitter?: string;
    albums?: Album[];
    tours?: ArtistTour[];
}
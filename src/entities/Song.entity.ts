import { SongType } from "../enums/SongType.enum";
import { Album } from "./Album.entity";
import { Artist } from "./Artist.entity";
import { SongCategory } from "./SongCategory.entity";


export interface Song {
    id?: number;
    title?: string;
    artists?: Artist[];
    album?: Album;
    type?: SongType;
    categories?: SongCategory[];
    photo?: string;
    releaseDate?: Date;
    lyrics?: string;
    audio?: string;
    video?: string;
    durationInSeconds?: number;
    orderInAlbum?: number;
    monthlyListenCount?: number;
}





















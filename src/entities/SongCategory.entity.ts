export interface SongCategory {
    id?: number;
    name?: string;
    display?: string;
    description?: string;
}
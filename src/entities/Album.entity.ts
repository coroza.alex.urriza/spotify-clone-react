import { Artist } from "./Artist.entity";



export interface Album {
    id?: number;
    name?: string;
    photo?: string;
    songs?: number[];
    artist?: Artist;
    copyright?: string;
    copyrightDate?: Date;
    releaseDate?: Date;
}
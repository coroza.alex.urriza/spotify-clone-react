export interface ArtistTour {
  id?: number;
  date?: Date;
  title?: string;
  venueName?: string;
  locationName?: string;
}
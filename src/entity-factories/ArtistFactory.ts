import { merge } from "lodash";
import { Artist } from "../entities/Artist.entity";
import { rand, randNumber, randParagraph, randSinger } from "@ngneat/falso";



export function ArtistFactory() {
    const create = (artist: Artist): Artist => {
        const random: Artist = {
            name: randSinger(),
            photo: `https://picsum.photos/150/150?randomData=${randNumber()}`,
            coverPhoto: `https://picsum.photos/300/200?randomData=${randNumber()}`,
            introText: randParagraph(),
            followersCount: randNumber({ min: 10000, max: 1000000 }),
            isVerified: true,
            monthlyListenersCount: randNumber({ min: 10000, max: 1000000 }),
            facebook: 'https://www.facebook.com/profile.php?id=100081813284987',
            instagram: 'https://www.instagram.com/_rick_rolled__/',
            twitter: 'https://twitter.com/rickroll?s=20'
        }
        
        return merge(artist, random);
    }

    return {
        create
    };
}
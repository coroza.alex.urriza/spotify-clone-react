import { merge } from "lodash";
import { Album } from "../entities/Album.entity";
import { randDrinks, randNumber, randPastDate } from "@ngneat/falso";



export function AlbumFactory() {
  const create = (album?: Partial<Album>): Album => {
    const random: Album = {
      name: randDrinks(),
      photo: `https://picsum.photos/150/150?randomData=${randNumber()}`,
      copyright: '℗ (P)2019 Sony Music Entertainment (Japan) Inc.',
      copyrightDate: randPastDate(),
      releaseDate: randPastDate()
    };
  
    return merge(album, random);
  }

  return {
    create
  };
}

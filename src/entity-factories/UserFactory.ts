import { merge } from "lodash";
import { User } from "../entities/User.entity";
import { randFullName } from "@ngneat/falso";



export function UserFactory() {
    const create = (user: User): User => {
        const random: User = {
            fullName: randFullName(),
            photo: 'https://imgs.smoothradio.com/images/191589?width=1200&crop=1_1&signature=KHg-WnaLlH9KsZwE-qYgxTkaSpU='
        };
    
        return merge(user, random);
    }

    return {
        create
    };
}
import { merge } from "lodash";
import { SongCategory } from "../entities/SongCategory.entity";
import { randMusicGenre, randParagraph } from "@ngneat/falso";

export function SongCategoryFactory() {
    const create = (songCategory: SongCategory): SongCategory => {
        const genre = randMusicGenre();
        
        const random: SongCategory = {
            name: genre,
            display: genre.toUpperCase(),
            description: randParagraph()
        };
    
        return merge(songCategory, random);
    }

    return {
        create
    };
}
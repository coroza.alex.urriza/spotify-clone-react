import { rand, randNumber, randPastDate, randSong } from "@ngneat/falso";
import { merge } from 'lodash';

import { Song } from "../entities/Song.entity";
import { SongType } from "../enums/SongType.enum";



export function SongFactory() {
  const create = (song?: Song): Song => {
    const random = {
      title: randSong(),
      type: rand([SongType.Livestream, SongType.Podcast, SongType.Song]),
      releaseDate: randPastDate(),
      photo: 'https://picsum.photos/300',
      lyrics: sampleLyrics,
      audio: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
      video: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
      durationInSeconds: randNumber({ min: 150, max: 350 }),
      monthlyListenCount: randNumber({ min: 10000, max: 10000000 }),
    };
  
    return merge(song, random);
  }


  const sampleLyrics = `We're no strangers to love
    You know the rules and so do I (do I)
    A full commitment's what I'm thinking of
    You wouldn't get this from any other guy
    I just wanna tell you how I'm feeling
    Gotta make you understand
    Never gonna give you up
    Never gonna let you down
    Never gonna run around and desert you
    Never gonna make you cry
    Never gonna say goodbye
    Never gonna tell a lie and hurt you
    We've known each other for so long
    Your heart's been aching, but you're too shy to say it (say it)
    Inside, we both know what's been going on (going on)
    We know the game and we're gonna play it
    And if you ask me how I'm feeling
    Don't tell me you're too blind to see
    Never gonna give you up
    Never gonna let you down
    Never gonna run around and desert you
    Never gonna make you cry
    Never gonna say goodbye
    Never gonna tell a lie and hurt you
    Never gonna give you up
    Never gonna let you down
    Never gonna run around and desert you
    Never gonna make you cry
    Never gonna say goodbye
    Never gonna tell a lie and hurt you
    We've known each other for so long
    Your heart's been aching, but you're too shy to say it (to say it)
    Inside, we both know what's been going on (going on)
    We know the game and we're gonna play it
    I just wanna tell you how I'm feeling
    Gotta make you understand
    Never gonna give you up
    Never gonna let you down
    Never gonna run around and desert you
    Never gonna make you cry
    Never gonna say goodbye
    Never gonna tell a lie and hurt you
    Never gonna give you up
    Never gonna let you down
    Never gonna run around and desert you
    Never gonna make you cry
    Never gonna say goodbye
    Never gonna tell a lie and hurt you
    Never gonna give you up
    Never gonna let you down
    Never gonna run around and desert you
    Never gonna make you cry
    Never gonna say goodbye
    Never gonna tell a lie and hurt you
  `;

  return {
    create
  };
}
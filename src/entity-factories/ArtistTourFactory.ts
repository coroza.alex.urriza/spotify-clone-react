import { merge } from "lodash";
import { randCounty, randFutureDate, randMovie } from "@ngneat/falso";
import { ArtistTour } from "../entities/ArtistTour.entity";



export function ArtistTourFactory() {
  const create = (tour?: ArtistTour): ArtistTour => {
    const random: ArtistTour = {
      date: randFutureDate(),
      title: randMovie(),
      venueName: 'Araneta Coliseum',
      locationName: randCounty()
    };
  
    return merge(tour, random);
  }

  return {
    create
  };
}

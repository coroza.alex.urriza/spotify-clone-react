import ColorThief from "colorthief";
import rgbHex from 'rgb-hex';


/**
 * Return the dominant color(hex) from an image element.
 */
export function useGetDominantColor(imgElement: HTMLImageElement): string {
    const colorThief = new ColorThief();

    if(imgElement?.complete) {
      const result = colorThief.getColor(imgElement);
      return `#${rgbHex(result[0], result[1], result[2])}`;
    } else {
      return '#000000';
    }
}
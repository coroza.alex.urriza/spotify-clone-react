import { useContext } from "react";
import { Playlist } from "../entities/Playlist.entity";
import { AppContext } from "../AppContext";


export function useIsPlaylistPlaying(playlist: Playlist): boolean {
    const { currentSong, isSongPlaying } = useContext(AppContext);
    const playlistSongIds = playlist?.songs?.map(song => song.id);

    if(isSongPlaying && playlistSongIds.includes(currentSong?.id)) {
        return true;
    } else {
        return false
    }
}
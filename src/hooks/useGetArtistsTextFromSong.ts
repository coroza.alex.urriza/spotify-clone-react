import { Song } from "../entities/Song.entity";

/**
 * Extract the artist text from a list of Artist[].
 * Get only the first 2 artists then combine them with a comma.
 * Append "and more" text if there are more than 3 artists.
 */
export function useGetArtistsTextFromSong(song: Song): string {
  if(!song) {
    return '';
  }
  
  let artistsText = '';
  const artistNames: string[] = [];

  song.artists?.forEach((artist, index) => {
    if(index < 3) {
      artistNames.push(artist.name);
    }
  })

  artistsText = artistNames.join(', ');

  if(song.artists?.length > 3) {
    artistsText = `${artistsText} and more`;
  }
  
  return artistsText;
}
import hexRgb from "hex-rgb";


/**
 * Return the background color in rgba syntax including the opacity.
 * @param backgroundColor hex color
 * @param scrollPosition scroll position(px) from the top of the scrolling div
 * @param maxScrollPosition max px where the opacity should be 1
 */
export function useBackgroundOpacityOnScroll(backgroundColor: string, scrollPosition: number, maxScrollPosition: number): string {
  const calcOpacity = (scrollPosition: number): number => {
    if(scrollPosition > maxScrollPosition) {
      return 1;
    } else {
      let opacity = (scrollPosition / maxScrollPosition);
      return opacity;
    }
  }

  const backgroundColorWithOpacity = () => {
    try {
      const rgb = hexRgb(backgroundColor);
      const result = `rgba(${rgb.red}, ${rgb.green}, ${rgb.blue}, ${calcOpacity(scrollPosition)})`;
      return result;
    } catch(e) {
      return 'rgba(0, 0, 0, 0)';
    }
  }

  return backgroundColorWithOpacity();
}
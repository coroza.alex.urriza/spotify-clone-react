import { Song } from "../entities/Song.entity";
import { useGetArtistsTextFromSong } from "./useGetArtistsTextFromSong";



describe('useGetArtistsTextFromSong', () => {
  test('should return the correct text for songs with LESS than 3 artists', () => {
    const song1: Song = {
      id: 1,
      artists: [
        { name: 'Nirvana' },
      ]
    };

    const song2: Song = {
      id: 2,
      artists: [
        { name: 'John Lennon' },
        { name: 'The Corrs' },
        { name: 'Linkin Park' },
      ]
    }
    
    const result1 = useGetArtistsTextFromSong(song1);
    const result2 = useGetArtistsTextFromSong(song2);
    expect(result1).toEqual('Nirvana');
    expect(result2).toEqual('John Lennon, The Corrs, Linkin Park');
  })


  test('should return the correct text for songs with MORE than 3 artists', () => {
    const song1: Song = {
      id: 2,
      artists: [
        { name: 'John Lennon' },
        { name: 'The Corrs' },
        { name: 'Linkin Park' },
        { name: 'My Chemical Romance' },
        { name: 'Batang Pusa' },
      ]
    }
    
    const result1 = useGetArtistsTextFromSong(song1);
    expect(result1).toEqual('John Lennon, The Corrs, Linkin Park and more');
  })


  test('should return empty if provided with an empty song', () => {
    const song: Song = null;
    const result = useGetArtistsTextFromSong(song);
    expect(result).toEqual('');
  })
})
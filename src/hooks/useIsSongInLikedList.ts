import { Song } from "../entities/Song.entity";



export function useIsSongInLikedList(song: Song, likedSongsByUser: Song[]): boolean {
  const likedSongIds = likedSongsByUser?.map(song => song.id) ?? [];

  if(likedSongIds.includes(song?.id)) {
    return true;
  } else {
    return false;
  }
}
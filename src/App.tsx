import './assets/scss/GlobalStyles.scss';
import './App.scss';

import { useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { SongApi } from './api/SongApi';
import { UserApi } from './api/UserApi';

import { AppContext, AppContextData, AppContextDefaultData } from './AppContext';

import { HomePage } from './pages/home-page/HomePage';
import { PlaybackBar } from './components/playback-bar/PlaybackBar';
import { SearchPanel } from './panels/search-panel/SearchPanel';
import { YourLibraryPanel } from './panels/your-library-panel/YourLibraryPanel';
import { NowPlayingPanel } from './panels/now-playing-panel/NowPlayingPanel';




function App() {
  const [currentSong, setCurrentSong] = useState(null);
  const [loggedInUser, setLoggedInUser] = useState(null);
  const [isSongPlaying, setIsSongPlaying] = useState(false);
  const [isNowPlayingViewOpen, setIsNowPlayingViewOpen] = useState(false);
  const [currentPlaylistName, setCurrentPlaylistName] = useState('');

  const appContextData: AppContextData = {
    ...AppContextDefaultData,
    isSongPlaying,
    setIsSongPlaying,
    isNowPlayingViewOpen,
    setIsNowPlayingViewOpen,
    loggedInUser,
    setLoggedInUser,
    currentSong,
    setCurrentSong,
    currentPlaylistName,
    setCurrentPlaylistName
  };
  
  useEffect(() => {
    loadInitialData();
  }, []);


  const loadInitialData = (): void => {
    Promise.all([
      SongApi.getCurrentSong(),
      UserApi.getLoggedInUser(),
    ]).then(data => {
      setCurrentSong(data[0]);
      setLoggedInUser(data[1]);
    });
  }
  
  
  return (
    <div className="App flex flex-column gap-2">
      <AppContext.Provider value={appContextData}>
        <BrowserRouter>
          <div className="main-content flex flex-row flex-grow-1 gap-2">
            <div className="left-side flex flex-column gap-2">
              <SearchPanel></SearchPanel>
              <YourLibraryPanel></YourLibraryPanel>
            </div>

            <Routes>
              <Route path="/" Component={HomePage}></Route>
            </Routes>

            <div className="right-side">
              <NowPlayingPanel></NowPlayingPanel>
            </div>
          </div>

          <PlaybackBar></PlaybackBar>
        </BrowserRouter>
      </AppContext.Provider>
    </div>
  )
}

export default App;

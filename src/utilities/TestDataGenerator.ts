import { rand, randNumber } from "@ngneat/falso";
import { Album } from "../entities/Album.entity";
import { Artist } from "../entities/Artist.entity";
import { Playlist } from "../entities/Playlist.entity";
import { Song } from "../entities/Song.entity";
import { SongCategory } from "../entities/SongCategory.entity";
import { User } from "../entities/User.entity";
import { AlbumFactory } from "../entity-factories/AlbumFactory";
import { ArtistFactory } from "../entity-factories/ArtistFactory";
import { PlaylistFactory } from "../entity-factories/PlaylistFactory";
import { SongCategoryFactory } from "../entity-factories/SongCategoryFactory";
import { SongFactory } from "../entity-factories/SongFactory";
import { UserFactory } from "../entity-factories/UserFactory";
import { ArtistTour } from "../entities/ArtistTour.entity";
import { ArtistTourFactory } from "../entity-factories/ArtistTourFactory";



const songs: Song[] = [];
const albums: Album[] = [];
const artists: Artist[] = [];
const playlists: Playlist[] = [];
const songCategories: SongCategory[] = [];
const users: User[] = [];
const artistTours: ArtistTour[] = [];
let loggedInUser: User = null;
let currentSong: Song = null;

const songsCount = 100;
const albumsCount = 45;
const artistsCount = 12;
const playlistsCount = 50;
const songCategoriesCount = 5;
const usersCount = 5;
const tourCount = 50;


// populate songs.
for(let i = 0; i < songsCount; i++) {
  const customData = { id: i + 1 };
  songs.push(SongFactory().create(customData));
}

// populate albums
for(let i = 0; i < albumsCount; i++) {
  const customData = { id: i + 1 };
  albums.push(AlbumFactory().create(customData));
}

// populate artists
for(let i = 0; i < artistsCount; i++) {
  const customData = { id: i + 1 };
  artists.push(ArtistFactory().create(customData));
}

// populate playlists
for(let i = 0; i < playlistsCount; i++) {
  const customData = { id: i + 1 };
  playlists.push(PlaylistFactory().create(customData));
}

// populate songCategories
for(let i = 0; i < songCategoriesCount; i++) {
  const customData = { id: i + 1 };
  songCategories.push(SongCategoryFactory().create(customData));
}

// populate users
for(let i = 0; i < usersCount; i++) {
  const customData = { id: i + 1 };
  users.push(UserFactory().create(customData));
}


// populate artist tours
for(let i = 0; i < tourCount;  i++) {
  const customData = { id: i+1 };
  artistTours.push(ArtistTourFactory().create(customData));
}


// Relationship data for Song
songs.forEach(song => {
  song.artists = [];
  
  const artistCount = randNumber({ min: 1, max: 3 });
  for(let i = 0; i < artistCount; i++) {
    song.artists.push(rand(artists));
  }
  
  song.album = rand(albums);
  song.categories = [rand(songCategories)];
});

// Relationship data for Album
albums.forEach(album => {
  album.artist = rand(artists);
});

// Relationship data for Artist
artists.forEach(artist => {
  artist.albums = [rand(albums), rand(albums), rand(albums)];
  artist.tours = [rand(artistTours), rand(artistTours), rand(artistTours), rand(artistTours)];
});


// Relationship data for Playlist
playlists.forEach(playlist => {
  playlist.createdBy = rand(users);
  playlist.songs = [];

  const songsCount = randNumber({ min: 2, max: 40 });
  for(let i = 0; i < songsCount; i++) {
    playlist.songs.push(rand(songs));
  }
});


// Relationship data for User
users.forEach(user => {
  user.ownPlaylists = [rand(playlists), rand(playlists)];
  user.likedPlaylists = [rand(playlists), rand(playlists), rand(playlists), rand(playlists), rand(playlists), rand(playlists), rand(playlists)];
  user.pinnedPlaylists = [rand(playlists), rand(playlists), rand(playlists), rand(playlists), rand(playlists), rand(playlists)];
  user.followedArtists = [rand(artists), rand(artists), rand(artists)];
  user.likedSongs = [rand(songs), rand(songs), rand(songs), rand(songs), rand(songs), rand(songs), rand(songs)];
});


// Populate loggedInUser
loggedInUser = rand(users);

// Populate currentSong
currentSong = songs[0];



export const TestDataGenerator = {
  songs,
  albums,
  artists,
  playlists,
  songCategories,
  users,
  loggedInUser,
  currentSong
}

console.log('TEST DATA', TestDataGenerator);

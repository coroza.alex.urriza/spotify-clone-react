import './ProgressBar.scss';


interface ProgressBarProps {
  maxValue: number;
  currentValue: number;
}



export function ProgressBar(props: ProgressBarProps) {
  const progress = (props.currentValue < props.maxValue) ? (props.currentValue / props.maxValue) * 100 : 100;
  
  
  return (
    <div className="ProgressBar">
      <div className="progress" style={{ width: `${progress}%` }}></div>
    </div>
  );
}
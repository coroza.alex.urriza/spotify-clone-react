import './RaisedButton.scss';


interface RaisedButtonProps {
  size?: number;
  backgroundColor?: string;
  children?: string | JSX.Element | JSX.Element[];
  style?: any;
  onClick?: () => void;
}



export function RaisedButton(props: RaisedButtonProps) {
  const buttonStyles = () => {
    const styles: any = props.style ?? {};
    
    if(props.size) {
      styles.height = `${props.size}px`;
      styles.width = `${props.size}px`;
    }

    if(props.backgroundColor) {
      styles.backgroundColor = props.backgroundColor;
    }
    
    return styles;
  }
  
  
  return (
    <button className="RaisedButton flex flex-row justify-content-center align-items-center" style={buttonStyles()} onClick={() => { props?.onClick ? props.onClick() : null }}>
      {props?.children}
    </button>
  );
}

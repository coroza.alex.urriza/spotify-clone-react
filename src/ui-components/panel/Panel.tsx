/**
 * Might remove this.
 * Im planning to just create a ui-component that is utilizing the components available in primereact and just restyle it there.
 */

import './Panel.scss';


interface Props {
    children?: string | JSX.Element | JSX.Element[];
}


export function AppPanel(props: Props) {
    return (
        <div className="AppPanel">
            {props.children}
        </div>
    );
}
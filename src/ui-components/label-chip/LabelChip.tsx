import './LabelChip.scss';


interface Props {
    children?: string | JSX.Element | JSX.Element[];
    isSelected?: boolean;
    onClick?: () => void;
}



export function LabelChip(props: Props) {
    const classIsSelected = (): string => {
        return (props.isSelected ? 'is-selected' : '')
    }


    const runOnClick = () => {
        if(props.onClick) {
            props.onClick();
        }
    }
    
    
    return (
        <div className={`LabelChip text-sm ${classIsSelected()}`} onClick={runOnClick}>{props.children}</div>
    );
}
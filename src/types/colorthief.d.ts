/**
 * Type declaration for colorthief plugin.
 * The problem is that colorthief doesnt have typescript types so we create our own(copied from others lol).
 */


declare module 'colorthief' {
  export type RGBColor = [number, number, number];
  export default class ColorThief {
    getColor: (img: HTMLImageElement | null, quality: number=10) => RGBColor;
    getPalette: (img: HTMLImageElement | null, colorCount: number=10, quality: number=10) => RGBColor[];
  }
}
import './LibraryPlaylistItem.scss';
import { Playlist } from '../../entities/Playlist.entity';
import { PlaylistType } from '../../enums/PlaylistType.enum';


interface Props {
    playlist: Playlist;
    pinnedPlaylistIds: number[];
}


export function LibraryPlaylistItem(props: Props) {
    const renderPinIcon = () => {
        if(props?.pinnedPlaylistIds?.includes(props?.playlist?.id)) {
            return <span className="pin-icon material-symbols-outlined">push_pin</span>;
        } else {
            return null;
        }
    }


    const getPlaylistTypeName = () => {
        const playlistTypeName = PlaylistType[props?.playlist?.type];
        return playlistTypeName;
    }


    const redirect = () => {
        alert(`Redirect to correct routes, depending on the playlist type! Redirect to ${getPlaylistTypeName()} page!`);
    }
    
    
    return (
        <div className="LibraryPlaylistItem flex flex-row align-items-center gap-2" onClick={redirect}>
            <div className="thumbnail">
                <img src={props?.playlist?.photo} />
            </div>

            <div className="details flex flex-column gap-1">
                <div className="name">{ props?.playlist?.name }</div>

                <div className="sub-details flex flex-row align-items-center gap-2 text-sm">
                    {renderPinIcon()}
                    <div>{ getPlaylistTypeName() }</div>
                    <span className="divider-icon material-symbols-outlined">circle</span>
                    <div>{ props?.playlist?.songs?.length ?? 0 } Songs</div>
                </div>
            </div>
        </div>
    );
}
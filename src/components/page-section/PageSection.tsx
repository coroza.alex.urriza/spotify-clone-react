import { Button } from 'primereact/button';

import './PageSection.scss';



interface PageSectionProps {
  title: string;
  children?: string | JSX.Element | JSX.Element[];
}


export function PageSection(props: PageSectionProps) {
  return (
    <div className="PageSection flex flex-column">
      <div className="header flex flex-row justify-content-between align-items-center">
        <div className="title text-2xl">{props.title}</div>
        <div className="actions">
          <Button className="text-sm" label="Show All" link onClick={() => { console.log('Redirect to another page that shows all the playlists under this suggestion.') }} />
        </div>
      </div>

      <div className="contents flex flex-row gap-3">
        {props.children}
      </div>
    </div>
  );
}
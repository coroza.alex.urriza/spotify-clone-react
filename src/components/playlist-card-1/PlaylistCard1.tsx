import { useRef, useState, useContext } from 'react';

import './PlaylistCard1.scss';

import { Playlist } from "../../entities/Playlist.entity";

import { AppContext } from '../../AppContext';

import { useGetDominantColor } from '../../hooks/useGetDominantColor';
import { useIsPlaylistPlaying } from '../../hooks/useIsPlaylistPlaying';

import { RaisedButton } from '../../ui-components/raised-button/RaisedButton';



interface PlaylistCard1Props {
  playlist: Playlist;
  onMouseEnter?: (hexColor: string) => void;
  onMouseLeave?: () => void;
}


export function PlaylistCard1(props: PlaylistCard1Props) {
  const { setIsSongPlaying, setCurrentSong, setCurrentPlaylistName } = useContext(AppContext);
  const imgRef = useRef();
  const [dominantColor, setDominantColor] = useState('');
  const isPlaylistPlaying = useIsPlaylistPlaying(props.playlist);

  
  const playFirstSongInPlaylist = (): void => {
    const firstSong = props.playlist.songs[0];
    setCurrentSong(firstSong);
    setIsSongPlaying(true);
    setCurrentPlaylistName(props.playlist.name);
  }
  
  
  const renderPlayButton = () => {
    const iconName = isPlaylistPlaying ? 'pause' : 'play_arrow';
    const style: any = {};

    // Always show the play/pause button if the playlist is playing
    if(isPlaylistPlaying) {
      style.opacity = 1;
    }

    return (
      <RaisedButton size={50} style={style} onClick={playFirstSongInPlaylist}>
        <span className="material-symbols-outlined text-3xl">{iconName}</span>
      </RaisedButton>
    );
  }
  
  
  return (
    <div className="PlaylistCard1 flex flex-row gap-3" onMouseEnter={() => { props?.onMouseEnter ? props.onMouseEnter(dominantColor) : null }} onMouseLeave={() => { props.onMouseLeave ? props.onMouseLeave() : null }}>
      <div className="image-container flex flex-row justify-content-center align-items-center">
        <img ref={imgRef} crossOrigin={'anonymous'} src={props.playlist.photo} onLoad={() => { setDominantColor(useGetDominantColor(imgRef.current)) }} />
      </div>

      <div className="details flex flex-row justify-content-between align-items-center flex-grow-1">
        <div className="name">{ props.playlist.name }</div>
        {renderPlayButton()}
      </div>
    </div>
  );
}
import { useContext } from "react";
import { uniq } from "lodash";
import ClampLines from "react-clamp-lines";

import './PlaylistCard2.scss';
import { PlaylistType } from "../../enums/PlaylistType.enum";
import { Playlist } from "../../entities/Playlist.entity";
import { Song } from "../../entities/Song.entity";

import { AppContext } from "../../AppContext";
import { useIsPlaylistPlaying } from "../../hooks/useIsPlaylistPlaying";
import { RaisedButton } from "../../ui-components/raised-button/RaisedButton";


interface PlaylistCard2Props {
  playlist: Playlist
}


export function PlaylistCard2(props: PlaylistCard2Props) {
  const { setIsSongPlaying, setCurrentSong, setCurrentPlaylistName } = useContext(AppContext);
  const isPlaylistPlaying = useIsPlaylistPlaying(props.playlist);
  
  
  const getArtistsText  = (playlistSongs: Song[]): string => {
    // Get all artists under each songs in a playlist
    const artists: string[] = [];
    playlistSongs.forEach(song => {
      song?.artists?.forEach(artist => {
        artists.push(artist.name);
      });
    });

    // Remove duplicate artist names
    const uniqueArtist = uniq(artists);
    
    // Display only 3 artist at max
    const artistsToDisplay: string[] = [];
    uniqueArtist.forEach((artistName, index) => {
      if(index < 3) {
        artistsToDisplay.push(artistName);
      }
    });

    let artistsText = artistsToDisplay.join(', ');

    // Add "and more" text if there are more than 3 artists
    if(uniqueArtist.length > 3) {
      artistsText = artistsText + ' and more';
    }

    return artistsText;
  }


  const playFirstSongInPlaylist = (): void => {
    const firstSong = props.playlist.songs[0];
    setCurrentSong(firstSong);
    setIsSongPlaying(true);
    setCurrentPlaylistName(props.playlist.name);
  }


  const determineImageContainerClassName = (): string => {
    if(props.playlist?.type === PlaylistType.Artist) {
      return 'artist';
    } else {
      return '';
    }
  }


  const renderPlayButton = () => {
    const iconName = isPlaylistPlaying ? 'pause' : 'play_arrow';
    const style: any = {};

    // Always show the play/pause button if the playlist is playing
    if(isPlaylistPlaying) {
      style.opacity = 1;
    }

    return (
      <RaisedButton size={50} style={style} onClick={playFirstSongInPlaylist}>
        <span className="material-symbols-outlined text-3xl">{iconName}</span>
      </RaisedButton>
    );
  }


  /**
   * Render either the artists text or the playList type depends on Playlist.type
   */
  const renderOtherDetails = () => {
    if(props.playlist?.type === PlaylistType.Artist) {
      return <div className="other-details text-sm">Artist</div>
    } else {
      return <ClampLines
        text={getArtistsText(props.playlist?.songs ?? [])}
        id="artists-text"
        className="artists text-sm"
        ellipsis="..."
        innerElement="div"
        buttons={false}
      />;
    }
  }
  
  
  return (
    <div className="PlaylistCard2 flex flex-column gap-3">
      {renderPlayButton()}
      
      <div className={`image-container ${determineImageContainerClassName()}`}>
        <img src={props.playlist?.photo} />
      </div>

      <div className="playlist-details flex flex-column gap-2">
        <div className="name text-lg">{props.playlist?.name}</div>
        {renderOtherDetails()}
      </div>
    </div>
  );
}
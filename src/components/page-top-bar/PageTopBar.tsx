import './PageTopBar.scss';
import { useBackgroundOpacityOnScroll } from '../../hooks/useBackgroundOpacityOnScroll';



interface PageTopBarProps {
  backgroundColor?: string;
  scrollPosition?: number;
}



export function PageTopBar(props: PageTopBarProps) {
  return (
    <div className="PageTopBar flex flex-row align-items-center gap-2" style={{ backgroundColor: useBackgroundOpacityOnScroll(props.backgroundColor, props.scrollPosition, 400)}}>
      <span className="nav-icon material-symbols-outlined">chevron_left</span>
      <span className="nav-icon disabled material-symbols-outlined">chevron_right</span>
      <div className="spacer flex-grow-1"></div>
      
      <div className="install-app flex flex-row align-items-center gap-1 text-sm">
        <span className="install-icon material-symbols-outlined">download</span>
        <div>Install App</div>
      </div>

      <div className="user-pic-container flex flex-row justify-content-center align-items-center">
        <img src={`https://picsum.photos/150/150`} />
      </div>
    </div>
  );
}
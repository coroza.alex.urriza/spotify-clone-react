import { Button } from "primereact/button";

import './NextInQueue.scss';
import { Song } from "../../entities/Song.entity";
import { useGetArtistsTextFromSong } from "../../hooks/useGetArtistsTextFromSong";


interface NextInQueueProps {
  nextSong?: Song;
}



export function NextInQueue(props: NextInQueueProps) {
  const artistsText = useGetArtistsTextFromSong(props.nextSong);
  

  const renderHeader = () => {
    if(props.nextSong) {
      return (
        <div className="header flex flex-row justify-content-between align-items-center">
          <div className="title text-sm">Next In Queue</div>
          <div className="actions">
            <Button className="text-sm" label="Open queue" link />
          </div>
        </div>
      )
    } 
    
    else {
      return (
        <div className="header flex align-items-center">
          <div className="title text-sm">Your queue is empty</div>
        </div>
      );
    }
  }


  const renderSongDetails = () => {
    if(props.nextSong) {
      return (
        <div className="next-song flex flex-row gap-3 align-items-center">
          <span className={`material-symbols-outlined text-lg`}>music_note</span>
          <div className="image-container">
            <img src={props?.nextSong?.photo} />
          </div>
          <div className="details flex flex-column">
            <div className="title text-sm" title={props?.nextSong?.title}>{props?.nextSong?.title}</div>
            <div className="artist text-xs" title={artistsText}>{artistsText}</div>
          </div>
        </div>
      );
    }

    else {
      return <Button className="search-for-new-button" label="Search for something new" severity="secondary" size="small" outlined />
    }
  }
  
  
  return (
    <div className="NextInQueue flex flex-column gap-3">
      {renderHeader()}
      {renderSongDetails()}
    </div>
  );
}
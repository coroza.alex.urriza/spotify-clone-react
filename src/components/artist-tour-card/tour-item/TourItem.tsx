import { format } from 'date-fns';
import './TourItem.scss';
import { ArtistTour } from '../../../entities/ArtistTour.entity';


interface TourItemProps {
  tour: ArtistTour;
}



export function TourItem(props: TourItemProps) {
  const renderCalendarDate = () => {
    return <div className="calendar-date flex flex-column align-items-center justify-content-center">
      <div className="month text-xs">{format(props.tour.date, 'MMM')}</div>  
      <div className="date text-2xl">{format(props.tour.date, 'dd')}</div>  
    </div>
  }


  const renderTourDetails = () => {
    return <div className="tour-details flex flex-column text-sm">
      <div className="title">{props.tour.title}</div>
      <div className="artist">Adele</div>
      <div className="other-details flex flex-row gap-1">
        <div className="date">{format(props.tour.date, 'iii h:mm a')}</div>
        <div className="text-bold">.</div>
        <div className="venue">{props.tour.venueName} at {props.tour.locationName}</div>
      </div>
    </div>
  }
  
  
  return (
    <div className="TourItem flex flex-row gap-3 align-items-center">
      {renderCalendarDate()}
      {renderTourDetails()}
    </div>
  )
}
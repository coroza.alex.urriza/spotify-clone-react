import { Button } from 'primereact/button';
import './ArtistTourCard.scss';
import { TourItem } from './tour-item/TourItem';
import { ArtistTour } from '../../entities/ArtistTour.entity';


interface ArtistTourCardProps {
  tours: ArtistTour[];
}



export function ArtistTourCard(props: ArtistTourCardProps) {
  return (
    <div className="ArtistTourCard flex flex-column gap-2">
      <div className="header flex flex-row justify-content-between align-items-center">
        <div className="title text-sm">On Tour</div>
        <div className="actions">
          <Button className="text-sm" label="Show all" link onClick={() => alert('Redirect to the list of Artist Tours Page!')} />
        </div>
      </div>

      <div className="tour-list flex flex-column gap-2">
        {props.tours?.map((tour, tourIndex) => <TourItem key={tourIndex} tour={tour}></TourItem>)}
      </div>
    </div>
  )
}
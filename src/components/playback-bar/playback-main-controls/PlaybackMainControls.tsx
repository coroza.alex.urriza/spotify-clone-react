import { useState, useContext, useEffect } from 'react';

import './PlaybackMainControls.scss';

import { AppContext } from '../../../AppContext';
import { RaisedButton } from '../../../ui-components/raised-button/RaisedButton';
import { ProgressBar } from '../../../ui-components/progress-bar/ProgressBar';

let songProgressInterval: number;



export function PlaybackMainControls() {
  const {isSongPlaying, setIsSongPlaying, currentSong} = useContext(AppContext);
  const [currentTime, setCurrentTime] = useState(0);
  const [shuffleEnabled, setShuffleEnabled] = useState(true);
  const [loopType, setLoopType] = useState<'repeatAll'|'repeatOne'|''>('');
  
  
  useEffect(() => {
    return () => {
      // Will execute on component unmount
      clearInterval(songProgressInterval);
    }
  }, []);


  useEffect(() => {
    setCurrentTime(0);
    startSongProgress();
  }, [currentSong])


  useEffect(() => {
    startSongProgress();
  }, [isSongPlaying])


  const startSongProgress = () => {
    clearInterval(songProgressInterval);

    if(isSongPlaying) {
      songProgressInterval = setInterval(() => {
        setCurrentTime(prevValue => prevValue + 1);
      }, 1000);
    }
  }
  
  
  const formatToMinuteSecond = (durationInSeconds: number): string => {
    if(!durationInSeconds) {
      return '00:00';
    }
    
    const minutes = Math.floor(durationInSeconds / 60);
    const seconds = durationInSeconds % 60;
    return `${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  }
  
  
  const toggleShuffle = () => {
    if(shuffleEnabled) {
      setShuffleEnabled(false);
    } else {
      setShuffleEnabled(true);
    }
  }


  const toggleLoopType = () => {
    if(loopType === 'repeatAll') {
      setLoopType('repeatOne');
    } else if (loopType === 'repeatOne') {
      setLoopType('');
    } else {
      setLoopType('repeatAll');
    }
  }


  const togglePlay = () => {
    if(isSongPlaying) {
      setIsSongPlaying(false);
    } else {
      setIsSongPlaying(true);
    }
  }
  
  
  const renderPlayButton = () => {
    const iconName = isSongPlaying ? 'pause' : 'play_arrow';

    return (
      <RaisedButton size={35} onClick={togglePlay}>
        <span className="material-symbols-outlined text-2xl">{iconName}</span>
      </RaisedButton>
    );
  }


  const renderPrevButton = () => {
    return <span title="Previous" className="prev-button material-symbols-outlined text-3xl">skip_previous</span>;
  }


  const renderNextButton = () => {
    return <span title="Next" className="next-button material-symbols-outlined text-3xl">skip_next</span>;
  }


  const renderShuffleButton = () => {
    let enabledClass = '';
    let title = '';
    
    if(shuffleEnabled) {
      enabledClass = 'active';
      title = 'Shuffle ON';
    } else {
      enabledClass = '';
    }
    
    return <span title={title} className={`shuffle-button material-symbols-outlined text-xl ${enabledClass}`} onClick={toggleShuffle}>shuffle</span>;
  }


  const renderLoopButton = () => {
    let iconName = 'repeat';
    let enabledClass = '';
    let title = '';

    if(loopType === 'repeatAll') {
      iconName = 'repeat';
      enabledClass = 'active';
      title = 'Repeat All';
    } else if(loopType === 'repeatOne') {
      iconName = 'repeat_one';
      enabledClass = 'active';
      title = 'Repeat One';
    } else {
      iconName = 'repeat';
      enabledClass = '';
      title = '';
    }
    
    return <span title={title} className={`loop-button material-symbols-outlined text-xl ${enabledClass}`} onClick={toggleLoopType}>{iconName}</span>;
  }
  
  
  return (
    <div className="main-controls flex flex-column flex-grow-1 justify-content-center gap-1">
        <div className="buttons flex flex-row gap-3 justify-content-center align-items-center">
          {renderShuffleButton()}
          {renderPrevButton()}
          {renderPlayButton()}
          {renderNextButton()}
          {renderLoopButton()}
        </div>

        <div className="playback-progress-indicator flex flex-row align-items-center gap-2">
          <div className="current-time text-xs">{formatToMinuteSecond(currentTime)}</div>
          <ProgressBar currentValue={currentTime} maxValue={currentSong?.durationInSeconds}></ProgressBar>
          <div className="max-time text-xs">{formatToMinuteSecond(currentSong?.durationInSeconds)}</div>
        </div>
      </div>
  );
}
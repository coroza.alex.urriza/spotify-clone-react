import { useContext, useState } from 'react';

import './PlaybackSongDetails.scss';

import { useIsSongInLikedList } from '../../../hooks/useIsSongInLikedList';
import { AppContext } from '../../../AppContext';



export function PlaybackSongDetails() {
  const [pictureInPicture, setPictureInPicture] = useState(false);
  const { currentSong, loggedInUser, setLoggedInUser } = useContext(AppContext);
  const isSongInLikedList = useIsSongInLikedList(currentSong, loggedInUser?.likedSongs);
  
  
  const getArtistsText = (): string => {
    const artistNames = currentSong?.artists?.map(artist => artist.name) ?? [];
    return artistNames.join(', ');
  }
  
  
  const toggleFavorite = () => {
    const loggedInUserCopy = {...loggedInUser};

    if(isSongInLikedList) {
      const likedSongs = loggedInUser?.likedSongs.filter(song => song !== currentSong.id);
      loggedInUserCopy.likedSongs = likedSongs;
    } else {
      loggedInUserCopy.likedSongs.push(currentSong);
    }

    setLoggedInUser(loggedInUserCopy);
  }


  const togglePictureInPicture = () => {
    if(pictureInPicture) {
      setPictureInPicture(false);
    } else {
      setPictureInPicture(true);
    }
  }
  
  
  const renderSaveToLibraryButton = () => {
    let enabledClass = '';
    let title = '';
    
    if(isSongInLikedList) {
      enabledClass = 'active';
      title = 'Save to Your Library';
    } else {
      enabledClass = '';
      title = 'Remove from Your Library';
    }
    
    return <span title={title} className={`material-symbols-outlined text-xl ${enabledClass}`} onClick={toggleFavorite}>favorite</span>;
  }


  const renderPictureInPictureButton = () => {
    const enabledClass = pictureInPicture ? 'active' : '';
    return <span title="Picture in Picture" className={`material-symbols-outlined text-xl ${enabledClass}`} onClick={togglePictureInPicture}>picture_in_picture</span>;
  }
  
  
  return (
    <div className="song-details flex flex-row  align-items-center gap-3">
      <div className="img-container">
        <img src={currentSong?.photo} />
      </div>
      <div className="title-and-artist flex flex-column justify-content-center">
        <div className="title text-sm">{currentSong?.title}</div>
        <div className="artists text-xs">{getArtistsText()}</div>
      </div>
      <div className="actions flex flex-row gap-2">
        {renderSaveToLibraryButton()}
        {renderPictureInPictureButton()}
      </div>
    </div>
  );
}
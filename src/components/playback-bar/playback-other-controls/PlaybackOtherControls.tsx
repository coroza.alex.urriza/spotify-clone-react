import { useContext, useState } from 'react';
import { Slider } from 'primereact/slider';

import './PlaybackOtherControls.scss';
import { AppContext } from '../../../AppContext';



export function PlaybackOtherControls() {
  const {isNowPlayingViewOpen, setIsNowPlayingViewOpen} = useContext(AppContext);
  const [isLyricsOpen, setIsLyricsOpen] = useState(false);
  const [isQueueOpen, setIsQueueOpen] = useState(false);
  const [volume, setVolume] = useState(100);
  
  
  /**
   * CONSIDER moving this into a separate component.
   */
  const renderVolumeSlider = () => {
    const volumeIcon = (volume) ? <span title="Mute" className={`material-symbols-outlined text-xl`}>volume_up</span> : <span title="Unmute" className={`material-symbols-outlined text-xl`}>volume_off</span>;
    
    return (
      <div className="volume-slider flex flex-row align-items-center gap-1" title={volume.toString()}>
        {volumeIcon}
        <Slider value={volume} onChange={(e: any) => setVolume(parseInt(e.value))} />
      </div>
    );
  }


  const renderNowPlayingViewButton = () => {
    const enabledClass = isNowPlayingViewOpen ? 'active' : '';
    return <span title="Now Playing View" className={`material-symbols-outlined text-xl ${enabledClass}`} onClick={() => {setIsNowPlayingViewOpen(!isNowPlayingViewOpen)}}>smart_display</span>;
  }

  const renderLyricsButton = () => {
    const enabledClass = isLyricsOpen ? 'active' : '';
    return <span title="Lyrics" className={`material-symbols-outlined text-xl ${enabledClass}`} onClick={() => {setIsLyricsOpen(!isLyricsOpen)}}>mic_external_on</span>;
  }

  const renderQueueButton = () => {
    const enabledClass = isQueueOpen ? 'active' : '';
    return <span title="Queue" className={`material-symbols-outlined text-xl ${enabledClass}`} onClick={() => {setIsQueueOpen(!isQueueOpen)}}>queue_music</span>;
  }
  
  
  return (
    <div className="other-controls flex flex-row gap-3">
      {renderNowPlayingViewButton()}
      {renderLyricsButton()}
      {renderQueueButton()}
      <span title="Connect to a Device" className={`material-symbols-outlined text-xl`}>speaker</span>
      {renderVolumeSlider()}
      <span title="Full screen" className={`material-symbols-outlined text-xl`}>fullscreen</span>
    </div>
  );
}
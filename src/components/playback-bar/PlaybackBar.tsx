import './PlaybackBar.scss';

import { PlaybackBarContext } from './PlaybackBarContext';

import { PlaybackMainControls } from './playback-main-controls/PlaybackMainControls';
import { PlaybackOtherControls } from './playback-other-controls/PlaybackOtherControls';
import { PlaybackSongDetails } from './playback-song-details/PlaybackSongDetails';



export function PlaybackBar() {
  return (
    <PlaybackBarContext.Provider value={{}}>
      <div className="PlaybackBar flex flex-row justify-content-between align-items-center gap-4">
        <PlaybackSongDetails></PlaybackSongDetails>
        <PlaybackMainControls></PlaybackMainControls>
        <PlaybackOtherControls></PlaybackOtherControls>
      </div>
    </PlaybackBarContext.Provider>
  );
}
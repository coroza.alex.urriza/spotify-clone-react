import './HomePage.scss';

import { useState, useEffect } from 'react';
import CustomScroll from 'react-custom-scroll';

import { PlaylistApi } from '../../api/PlaylistApi';

import { PageTopBar } from '../../components/page-top-bar/PageTopBar';
import { PageSection } from '../../components/page-section/PageSection';
import { PlaylistCard1 } from '../../components/playlist-card-1/PlaylistCard1';
import { PlaylistCard2 } from '../../components/playlist-card-2/PlaylistCard2';




export function HomePage() {
  const defaultColor = '#183164';
  const [scrollPosition, setScrollPosition] = useState(0);
  const [backgroundColor, setBackgroundColor] = useState(defaultColor);
  const [playlists, setPlaylists] = useState([]);


  useEffect(() => {
    loadPlaylistSuggestions();
  }, []);


  const loadPlaylistSuggestions = (): void => {
    PlaylistApi.getPlaylists()
    .then(playlists => {
      setPlaylists(playlists);
    });
  }


  const renderGreetingsPlaylists = () => {
    const contentPlaylists = playlists?.map((playlist, index) => {
      if(index < 6) { /* We want to show 6 items only */
        return <PlaylistCard1 key={index} playlist={playlist} onMouseEnter={(dominantColor) => { setBackgroundColor(dominantColor) }} onMouseLeave={() => { setBackgroundColor(defaultColor) }}></PlaylistCard1>;
      }
    })

    return contentPlaylists;
  }
  
  
  return (
    <div className="HomePage flex flex-column gap-3">
      <PageTopBar backgroundColor={backgroundColor} scrollPosition={scrollPosition}></PageTopBar>

      <CustomScroll heightRelativeToParent="100%" allowOuterScroll={true} onScroll={(scrollEvent) => { setScrollPosition(scrollEvent?.target?.scrollTop) }}>
        <div className="greetings-section flex flex-column gap-4" style={{ backgroundColor: backgroundColor }}>
          <div className="greeting text-3xl">Good afternoon</div>
          <div className="fake-gradient"></div>
          <div className="playlists flex flex-row flex-wrap justify-content-center gap-3">
            {renderGreetingsPlaylists()}
          </div>
        </div>
        
        <div className="page-content flex flex-column gap-5">
          <PageSection title="Made for Alex Coroza">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>
          
          <PageSection title="Pop in the Philippines">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Top Mixes">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Fresh Finds">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Jump Back In">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>

          <PageSection title="Your Favorite Artists">
            <PlaylistCard2 playlist={playlists[0]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[1]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[2]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[3]}></PlaylistCard2>
            <PlaylistCard2 playlist={playlists[4]}></PlaylistCard2>
          </PageSection>
        </div>
      </CustomScroll>
    </div>
  );
}
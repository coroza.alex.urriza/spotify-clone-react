export enum PlaylistType {
    All = 1,
    Playlist = 2,
    Artist = 3,
    Album = 4,
    Suggestion = 5
}
export enum SongType {
    Song = 1,
    Podcast = 2,
    Livestream = 3
}
import { Artist } from "../entities/Artist.entity";
import { TestDataGenerator } from "../utilities/TestDataGenerator";

const testData = TestDataGenerator;


export const ArtistApi = {
  getArtists: (): Promise<Artist[]> => {
    return Promise.resolve(testData.artists);
  }
};
import { SongCategory } from "../entities/SongCategory.entity";
import { TestDataGenerator } from "../utilities/TestDataGenerator";

const testData = TestDataGenerator;


export const SongCategoryApi = {
  getSongCategories: (): Promise<SongCategory[]> => {
    return Promise.resolve(testData.songCategories);
  }
};
import { Song } from "../entities/Song.entity";
import { TestDataGenerator } from "../utilities/TestDataGenerator";


const testData = TestDataGenerator;


export const SongApi = {
  getSongs: (): Promise<Song[]> => {
    return Promise.resolve(testData.songs);
  },
  
  getCurrentSong: (): Promise<Song> => {
    return Promise.resolve(testData.currentSong);
  }
};
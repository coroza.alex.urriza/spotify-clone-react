import { User } from "../entities/User.entity";
import { TestDataGenerator } from "../utilities/TestDataGenerator";

const testData = TestDataGenerator;


export const UserApi = {
  getUsers: (): Promise<User[]> => {
    return Promise.resolve(testData.users);
  },


  getLoggedInUser: (): Promise<User> => {
    return Promise.resolve(testData.loggedInUser);
  }
};
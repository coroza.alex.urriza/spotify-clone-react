import { Playlist } from "../entities/Playlist.entity";
import { TestDataGenerator } from "../utilities/TestDataGenerator";

const testData = TestDataGenerator;


export const PlaylistApi = {
  getPlaylists: (): Promise<Playlist[]> => {
    return Promise.resolve(testData.playlists);
  }
};
import { Album } from "../entities/Album.entity";
import { TestDataGenerator } from "../utilities/TestDataGenerator";


const testData = TestDataGenerator;


export const AlbumApi = {
  getAlbums: (): Promise<Album[]> => {
    return Promise.resolve(testData.albums);
  }
};